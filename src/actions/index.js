import {api} from '../api'
import {
	GET_SUGGESTION_REQUEST,
	GET_SUGGESTION_SUCCESS,
	GET_SUGGESTION_FAILURE,
	SELECT_SUGGESTION,
	UPDATE_QUERY_STRING,
	RESET_SEARCH_BAR
} from '../actionTypes'

export const actions = {
	updateQueryString,
	getSuggestion,
	selectSuggestion,
	reset
};

let abortController = new AbortController();

function getSuggestion(query) {
	abortController.abort();
	abortController = new AbortController();
	
	return dispatch => {
		dispatch(request());
		api.getSuggestion(query, abortController.signal)
		.then(async result => {
			dispatch(success(await result.json()));
		})
		.catch(() => dispatch(failure('Ошибка запроса')))
	};
	
	function request() {
		return {type: GET_SUGGESTION_REQUEST}
	}
	
	function success(result) {
		return {type: GET_SUGGESTION_SUCCESS, result}
	}
	
	function failure(error) {
		return {type: GET_SUGGESTION_FAILURE, error}
	}
}

function selectSuggestion(value) {
	return {type: SELECT_SUGGESTION, value};
}

function updateQueryString(value) {
	return {type: UPDATE_QUERY_STRING, value};
}

function reset() {
	abortController.abort();
	return {type: RESET_SEARCH_BAR};
}
