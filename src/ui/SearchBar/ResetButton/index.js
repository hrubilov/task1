import React from 'react'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faTimes} from '@fortawesome/free-solid-svg-icons'
import styles from './styles.module.scss'
import cl from 'classnames'

const ResetButton = ({show, onClick}) => (
	<div className={cl(styles.reset, show ? styles.show : null)} onClick={onClick}>
		<FontAwesomeIcon icon={faTimes}/>
	</div>
);

export default ResetButton;
