import React from 'react'
import Input from './Input'
import Dropdown from './Dropdown'
import ResetButton from './ResetButton'
import styles from './styles.module.scss'

const SearchBar = ({items, value, onInput, onSelect, onReset}) => (
	<div className={styles.container} onClick={handleClick}>
		<Input
			value={value}
			onChange={onInput}
		/>
		<Dropdown
			show={!!items.length}
			items={items}
			onSelect={onSelect}
		/>
		<ResetButton
			show={!!value}
			onClick={onReset}
		/>
	</div>
);

function handleClick(e) {
	e.stopPropagation();
}

export default SearchBar
