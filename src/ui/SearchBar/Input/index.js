import React from "react";
import styles from './styles.module.scss'

const Input = ({value, onChange}) => (
	<input
		name={'search'}
		value={value}
		type={'text'}
		placeholder={'Поиск по магазину'}
		autoComplete={'off'}
		className={styles.input}
		onChange={onChange}
	/>);

export default Input;
