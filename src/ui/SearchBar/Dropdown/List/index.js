import React from 'react'
import Item from '../Item'
import styles from './styles.module.scss'

const List = ({items, onSelect}) => (
	<div className={styles.list}>
		{items.map((data, i) =>
			<Item
				key={i}
				content={data}
				onClick={() => onSelect(data)}
			/>)}
	</div>
);

export default List;
