import React from 'react'
import List from './List'
import cl from "classnames";
import styles from './styles.module.scss'

const Dropdown = ({show, ...props}) => (
	<div className={cl(styles.dropdown, show ? styles.show : '')}>
		<List {...props}/>
	</div>
);

export default Dropdown;
