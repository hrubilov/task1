import React from 'react'
import styles from './styles.module.scss'

const Item = ({content, onClick}) => (
	<div className={styles.item} onClick={onClick}>
		{content}
	</div>
);

export default Item;
