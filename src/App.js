import React from 'react';
import SearchBar from './ui/SearchBar'
import './App.css'
import {actions} from "./actions";
import {connect} from "react-redux";
import {debounce, extractSuggestionItems} from "./helpers";

function App(props) {
	const {items, value} = props;
	
	function handleInput(e) {
		const {value} = e.target;
		props.updateQueryString(value);
		value && debounce(() => props.getSuggestion(value), 800)();
	}
	
	function handleSelect(value) {
		props.selectSuggestion(value);
	}
	
	function handleReset() {
		props.resetSearchBar();
	}
	
	return (
		<div className="App" onClick={props.resetSearchBar}>
			<SearchBar
				items={items}
				value={value}
				onInput={handleInput}
				onSelect={handleSelect}
				onReset={handleReset}
			/>
		</div>
	);
}

const mapDispatchToProps = {
	updateQueryString: actions.updateQueryString,
	getSuggestion: actions.getSuggestion,
	selectSuggestion: actions.selectSuggestion,
	resetSearchBar: actions.reset
};

function mapStateToProps(state) {
	const {value, suggestion} = state;
	const items = extractSuggestionItems(suggestion);
	return {items, value}
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
