export function debounce(f, ms) {
	let isCooldown = false;
	
	return function () {
		if (isCooldown) return;
		f.apply(this, arguments);
		isCooldown = true;
		setTimeout(() => isCooldown = false, ms);
	};
}

export function extractSuggestionItems(suggestions) {
	const {categories} = suggestions;
	const res = categories ? categories.map(item => item.name) : [];
	return [...new Set(res)];
}
