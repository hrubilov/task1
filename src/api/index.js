const url = new URL('https://api.savetime.net/');

export const api = {
	async getSuggestion(query, signal) {
		url.pathname = 'v1/client/suggest/item';
		url.search = `?q=${query}`;
		return await fetch(url, {
			method: 'GET',
			signal
		});
	}
};


