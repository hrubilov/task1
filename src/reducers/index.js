import {
	GET_SUGGESTION_REQUEST,
	GET_SUGGESTION_SUCCESS,
	GET_SUGGESTION_FAILURE,
	SELECT_SUGGESTION,
	UPDATE_QUERY_STRING,
	RESET_SEARCH_BAR
} from '../actionTypes'

const initialState = {
	value: '',
	suggestion: {}
};

function searchBar(state = initialState, action = {}) {
	switch (action.type) {
		case GET_SUGGESTION_REQUEST:
			return {
				...state
			};
		case GET_SUGGESTION_SUCCESS:
			return {
				...state,
				suggestion: action.result
			};
		case GET_SUGGESTION_FAILURE:
			return {
				...state
			};
		case UPDATE_QUERY_STRING:
			return {
				...state,
				value: action.value
			};
		case SELECT_SUGGESTION:
			return {
				...state,
				value: action.value,
				suggestion: {}
			};
		case RESET_SEARCH_BAR:
			return {
				...initialState
			};
		default:
			return state;
	}
}

export default searchBar;
